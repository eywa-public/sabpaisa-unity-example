using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AlertViewCallback : AndroidJavaProxy
        {
                const string pluginName = "com.sabpaisa.gateway.android.sdk.SabPaisaGatewayUnity";
            private System.Action<string> alertHandler;

            public AlertViewCallback(System.Action<string> alertHandlerIn) : base (pluginName + "$AlertViewCallback")
            {
                alertHandler = alertHandlerIn;
            }
            public void onButtonTapped(string index)
            {
                Debug.Log("Button tapped: " + index);
                if (alertHandler != null)
                    alertHandler(index);
            }
        }