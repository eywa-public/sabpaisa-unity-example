#import "UnityAppController.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SabPaisa_IOS_Sdk/SabPaisa_IOS_Sdk-Swift.h"
#include <stdio.h>

typedef void (*OnTimeOutCallback)(char* message);

extern UIViewController *UnityGetGLViewController();

@interface iOSPlugin : NSObject

@end


extern "C"
{
    void _ShowAlert(const char *amount,const char *firstName,const char *lastName,const char *mobileNumber,const char *emailId,const char *clientCode,const char *aesAPIKEY,const char *aesAPIIV,const char *transUserName,const char *transPassword,const char *initUrl,const char *baseUrl,const char *transactionEnqUrl,const char *transactionId,const char *callbackURL,
    OnTimeOutCallback callback)
    {
        // [iOSPlugin alertView:[NSString stringWithUTF8String:amount] firstName:[NSString stringWithUTF8String:firstName] lastName:[NSString stringWithUTF8String:lastName] mobileNumber:[NSString stringWithUTF8String:mobileNumber] emailId:[NSString stringWithUTF8String:emailId] clientCode:[NSString stringWithUTF8String:clientCode] aesAPIKEY:[NSString stringWithUTF8String:aesAPIKEY] aesAPIIV:[NSString stringWithUTF8String:aesAPIIV] transUserName:[NSString stringWithUTF8String:transUserName] transPassword:[NSString stringWithUTF8String:transPassword] initUrl:[NSString stringWithUTF8String:initUrl] baseUrl:[NSString stringWithUTF8String:baseUrl] transactionEnqUrl:[NSString stringWithUTF8String:transactionEnqUrl] txnId:[NSString stringWithUTF8String:transactionId] callbackURL:[NSString stringWithUTF8String:callbackURL]];
    
     NSBundle *bundle = [NSBundle bundleWithIdentifier:@"com.eywa.ios.SabPaisa-IOS-Sdk"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:bundle];
    
    // NSLog(@"aesAPIKEY"+aesAPIKEY);
    // NSLog(@"aesIV"+aesAPIIV);
    
    InitialLoadViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"InitialLoadViewController_Identifier"];
    
    SdkInitModel *sdkInitModel = [[SdkInitModel alloc] initWithFirstName:[NSString stringWithUTF8String:firstName] lastName:[NSString stringWithUTF8String:lastName] secKey:[NSString stringWithUTF8String:aesAPIKEY] secInivialVector:[NSString stringWithUTF8String:aesAPIIV] transUserName:[NSString stringWithUTF8String: transUserName] transUserPassword:[NSString stringWithUTF8String:transPassword] clientCode:[NSString stringWithUTF8String:clientCode] amount:[NSString stringWithUTF8String:amount] emailAddress:[NSString stringWithUTF8String:emailId] mobileNumber:[NSString stringWithUTF8String:mobileNumber] isProd:@"true" udf1:@"" udf2:@"" udf3:@"" udf4:@"" udf5:@"" udf6:@"" udf7:@"" udf8:@"" udf9:@"" udf10:@"" udf11:@"" udf12:@"" udf13:@"" udf14:@"" udf15:@"" udf16:@"" udf17:@"" udf18:@"" udf19:@"" udf20:@"" payerAddress:@"" amountType:@"" mcc:@"" transDate:@"" programId:@"" baseUrl:[NSString stringWithUTF8String:baseUrl] initiUrl:[NSString stringWithUTF8String:initUrl] transactionEnquiryUrl:[NSString stringWithUTF8String:transactionEnqUrl] salutation:@"Hi" sabpaisaPaymentScreenEnabled:false transactionId:[NSString stringWithUTF8String:transactionId] callbackUrl:[NSString stringWithUTF8String:callbackURL]];
    [vc setSdkInitModel:sdkInitModel];
    
    vc.callbackUnity = ^(NSString *response) {
        NSLog(@"---------------Final Response To USER------------");
        NSLog(@"Response : %@", response);
        const char *command = [response UTF8String];
        char *s=(char *)command;
        callback(s);
    };


    [UnityGetGLViewController() presentViewController:vc animated:YES completion:nil];
    
    }
    
    
}
