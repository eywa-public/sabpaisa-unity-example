using System;
using System.Runtime.InteropServices;
using UnityEngine;
using AOT;

public class iOSPlugin : MonoBehaviour
{
    private const string NOT_SUPPORTED = "not supported on this platform";

    
    #if UNITY_IOS

    [DllImport("__Internal")]
    private static extern void _ShowAlert(string amount,string firstName,
    string lastName,string mobileNumber,string emailId,string clientCode,string aesAPIKEY,string aesAPIIV,string transUserName,string transPassword,string initUrl,string baseUrl,string transactionEnqUrl,string transactionId,string callbackURL,Action<string> action );

    public static void ShowAlert(string amount,string firstName,
    string lastName,string mobileNumber,string emailId,string clientCode,string aesAPIKEY,string aesAPIIV,string transUserName,string transPassword,string initUrl,string baseUrl,string transactionEnqUrl,string transactionId,string callbackURL,AlertViewCallback handler = null)
    {
        _ShowAlert(amount,firstName,lastName,mobileNumber,emailId,clientCode,aesAPIKEY,aesAPIIV,transUserName,transPassword,initUrl,baseUrl,transactionEnqUrl,transactionId,callbackURL,CSSharpCallback);
    }

     [MonoPInvokeCallback(typeof(Action<string>))]
    public static void CSSharpCallback(string message)
    {
       Debug.Log($"C# callback received \"{message}\"");
       UIBindings.callIosCallback(message);
    }

    #else

    const string pluginName = "com.sabpaisa.gateway.android.sdk.SabPaisaGatewayUnity";
    static AndroidJavaClass _pluginClass;
    static AndroidJavaObject _pluginInstance;
    static AndroidJavaObject activity;
    // class IPaymentSuccessCallBack<TransactionResponsesModel> : AndroidJavaProxy
    // {
    // private System.Action<int> alertHandler;
    // }
    public static AndroidJavaClass PluginClass
    {
        get{
            if(_pluginClass == null){
                _pluginClass = new AndroidJavaClass(pluginName);
                if(activity == null){
                    AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                    activity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
                    //refence code to add this activity in static method
                    _pluginClass.SetStatic<AndroidJavaObject>("mainActivity",activity);
                }

            }
            return _pluginClass;
        }
    }

    public static AndroidJavaObject PluginInstance{
        get{
            if(_pluginInstance == null){
                _pluginInstance = PluginClass.CallStatic<AndroidJavaObject>("getInstance");
            }

            return _pluginInstance;

        }
    }
    public static void ShowAlert(string amount,string firstName,
    string lastName,string mobileNumber,string emailId,string clientCode,string aesAPIKEY,string aesAPIIV,string transUserName,string transPassword,string initUrl,string baseUrl,string transactionEnqUrl,string transactionId,string callbackURL, AlertViewCallback handler = null)
    {
        if(Application.platform == RuntimePlatform.Android){
            PluginInstance.Call("init",new object[] {amount,firstName,lastName,mobileNumber,emailId,clientCode,aesAPIKEY,aesAPIIV,transUserName,transPassword,initUrl,baseUrl,transactionEnqUrl,transactionId,callbackURL,handler});
        }
    }

    

    #endif
}