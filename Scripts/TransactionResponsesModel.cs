public class TransactionResponsesModel{

    public string payerName;
    public string payerEmail ;
    public string payerMobile ;
    public string clientTxnId ;
    public string payerAddress ;
    public string amount ;
    public string clientCode ;
    public string paidAmount ;
    public string paymentMode ;
    public string bankName ;
    public string amountType ;
    public string status ;
    public string statusCode ;
    public string challanNumber ;
    public string sabpaisaTxnId ;
    public string sabpaisaMessage ;
    public string bankMessage ;
    public string bankErrorCode ;
    public string sabpaisaErrorCode ;
    public string bankTxnId ;
    public string transDate ;
    public string udf1 ;
    public string udf2 ;
    public string udf3 ;
    public string udf4 ;
    public string udf5 ;
    public string udf6 ;
    public string udf7 ;
    public string udf8 ;
    public string udf9 ;
    public string udf10 ;
    public string udf11 ;
    public string udf12 ;
    public string udf13 ;
    public string udf14 ;
    public string udf15 ;
    public string udf16 ;
    public string udf17 ;
    public string udf18 ;
    public string udf19 ;
    public string udf20 ;

    }