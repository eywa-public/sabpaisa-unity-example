using UnityEngine;
using UnityEngine.UI;


public class UIBindings : MonoBehaviour
{
     const string ICLOUD_KEY = "myAppKey";
    const string pluginName = "com.sabpaisa.gateway.android.sdk.SabPaisaGatewayUnity";
     public static string amount = "105";
     static string firstName = "test";
     static string lastName = "testlastname";
     static string mobileNumber = "7432323211";
     static string emailId = "asd@gmail.com";
     static string clientCode = "LPSD1";
     string aesAPIKEY = "QVMtR1JBUy1QUk9E";
     string aesAPIIV = "1234567890123456";
     string transUserName = "Abh789@sp";
     string transPassword = "P8c3WQ7ei";

     string initUrl = "https://securepay.sabpaisa.in/SabPaisa/sabPaisaInit?v=1";
     string baseUrl = "https://securepay.sabpaisa.in";
     string transactionEnqUrl = "https://txnenquiry.sabpaisa.in";

    //this value should not be empty default is localhost
     string callbackURL = "https://localhost/Response";
     string transactionId = "";

    [SerializeField]
     Button showAlertButton;

     public static TransactionResponsesModel transactionResponsesModelGlobal;

   
    void Start()  
    {
        showAlertButton.onClick.AddListener(ShowBasicAlert);
    }

    void ShowBasicAlert() 
    {

        iOSPlugin.ShowAlert(amount,firstName,lastName,mobileNumber,emailId,clientCode,aesAPIKEY,aesAPIIV,transUserName,transPassword,initUrl,baseUrl,transactionEnqUrl,transactionId,callbackURL
        ,new AlertViewCallback((string obj) => {
				Debug.Log("Local Handler called: " + obj);
                TransactionResponsesModel transactionResponsesModel= JsonUtility.FromJson<TransactionResponsesModel>(obj);
                Debug.Log(transactionResponsesModel.payerName);
                transactionResponsesModelGlobal = transactionResponsesModel;
			}));
          Debug.Log("Transaction Started");
    }

    public static void callIosCallback(string obj){
        		Debug.Log("Local Handler called: " + obj);
                TransactionResponsesModel transactionResponsesModel= JsonUtility.FromJson<TransactionResponsesModel>(obj);
                Debug.Log(transactionResponsesModel.payerName);
                transactionResponsesModelGlobal = transactionResponsesModel;
    }

    


}